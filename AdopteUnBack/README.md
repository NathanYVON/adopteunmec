AdopteUnBack
============

A Symfony project created on October 7, 2019, 10:01 am.




To create database:

    - connect to mysql:
        mysql -u [user] -p

    - in mysql, drop the existing database:
        drop database [database];

    - in a regular shell, run this:
        php bin/console d:d:c
        php bin/console d:s:u --force
    
    - in mysql, select the newly created base:
        use [database];
    
    - in mysql, execute dump.sql:
        source [full path to dump.sql]